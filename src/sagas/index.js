// root sagas
import { all, takeEvery } from "redux-saga/effects";

import {
  FETCH_COUNTRIES_REQUESTED,
  SUBMIT_COUNTRY_REQUESTED,
  FETCH_COUNTRY_REQUESTED,
  DELETE_COUNTRY_REQUESTED,
} from "../actions/countries";
import {
  FETCH_PERSONS_REQUESTED,
  SUBMIT_PERSON_REQUESTED,
  FETCH_PERSON_REQUESTED,
  DELETE_PERSON_REQUESTED,
} from "../actions/persons";
import {
  FETCH_CARS_REQUESTED,
  SUBMIT_CAR_REQUESTED,
  FETCH_CAR_REQUESTED,
  DELETE_CAR_REQUESTED,
} from "../actions/cars";
import {
  FETCH_PROVINCES_REQUESTED,
  SUBMIT_PROVINCE_REQUESTED,
  FETCH_PROVINCE_REQUESTED,
  DELETE_PROVINCE_REQUESTED,
} from "../actions/provinces";
import {
  FETCH_CLUBS_REQUESTED,
  SUBMIT_CLUB_REQUESTED,
  FETCH_CLUB_REQUESTED,
  DELETE_CLUB_REQUESTED,
} from "../actions/clubs";

import {
  fetchCountries,
  submitCountry,
  fetchCountry,
  deleteCountry,
} from "./countries";
import {
  fetchPersons,
  submitPerson,
  fetchPerson,
  deletePerson,
} from "./persons";
import { fetchCars, submitCar, fetchCar, deleteCar } from "./cars";
import {
  fetchProvinces,
  submitProvince,
  fetchProvince,
  deleteProvince,
} from "./provinces";
import { fetchClubs, submitClub, fetchClub, deleteClub } from "./clubs";

//function* es una funcion de escucha, y es global
// () => es una funcion statica y no puede hacer la escucha
//yiel es una funcion de llamada

export default function* root() {
  yield all([
    takeEvery(FETCH_COUNTRIES_REQUESTED, fetchCountries),
    takeEvery(SUBMIT_COUNTRY_REQUESTED, submitCountry),
    takeEvery(FETCH_COUNTRY_REQUESTED, fetchCountry),
    takeEvery(DELETE_COUNTRY_REQUESTED, deleteCountry),

    takeEvery(FETCH_PROVINCES_REQUESTED, fetchProvinces),
    takeEvery(SUBMIT_PROVINCE_REQUESTED, submitProvince),
    takeEvery(FETCH_PROVINCE_REQUESTED, fetchProvince),
    takeEvery(DELETE_PROVINCE_REQUESTED, deleteProvince),

    takeEvery(FETCH_CARS_REQUESTED, fetchCars),
    takeEvery(SUBMIT_CAR_REQUESTED, submitCar),
    takeEvery(FETCH_CAR_REQUESTED, fetchCar),
    takeEvery(DELETE_CAR_REQUESTED, deleteCar),

    takeEvery(FETCH_CLUBS_REQUESTED, fetchClubs),
    takeEvery(SUBMIT_CLUB_REQUESTED, submitClub),
    takeEvery(FETCH_CLUB_REQUESTED, fetchClub),
    takeEvery(DELETE_CLUB_REQUESTED, deleteClub),

    takeEvery(FETCH_PERSONS_REQUESTED, fetchPersons),
    takeEvery(SUBMIT_PERSON_REQUESTED, submitPerson),
    takeEvery(FETCH_PERSON_REQUESTED, fetchPerson),
    takeEvery(DELETE_PERSON_REQUESTED, deletePerson),
  ]);
}
