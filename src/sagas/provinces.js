import { call, put, select } from "redux-saga/effects";
import ProvinceService from "../services/provinces";
import {
  fetchProvincesSucceeded,
  submitProvinceSucceeded,
  fetchProvinceSucceeded,
  deleteProvinceSucceeded,
} from "../actions/provinces";

export function* fetchProvinces({ filter }) {
  const provinces = yield call(ProvinceService.apiCall, filter);
  yield put(fetchProvincesSucceeded(provinces));
}

export function* submitProvince() {
  const { currentProvinces } = yield select((state) => state.provinces);
  const { status, data } = yield call(
    ProvinceService.apiSave,
    currentProvinces
  );
  yield put(submitProvinceSucceeded(status, data));
}

export function* fetchProvince({ id }) {
  const province = yield call(ProvinceService.apiCallOne, id);
  yield put(fetchProvinceSucceeded(province));
}

export function* deleteProvince({ id }) {
  yield call(ProvinceService.apiDelete, id);
  yield put(deleteProvinceSucceeded(true));
  yield call(fetchProvinces, {});
}
