import { call, put, select } from "redux-saga/effects";
import PersonService from "../services/persons";
import {
  fetchPersonsSucceeded,
  submitPersonSucceeded,
  fetchPersonSucceeded,
  deletePersonSucceeded,
} from "../actions/persons";

export function* fetchPersons({ filter }) {
  const persons = yield call(PersonService.apiCall, filter);
  yield put(fetchPersonsSucceeded(persons));
}

export function* submitPerson() {
  const { currentPersons } = yield select((state) => state.persons);
  const { status, data } = yield call(PersonService.apiSave, currentPersons);
  yield put(submitPersonSucceeded(status, data));
}

export function* fetchPerson({ id }) {
  const person = yield call(PersonService.apiCallOne, id);
  yield put(fetchPersonSucceeded(person));
}

export function* deletePerson({ id }) {
  yield call(PersonService.apiDelete, id);
  yield put(deletePersonSucceeded(true));
  yield call(fetchPersons, {});
}
