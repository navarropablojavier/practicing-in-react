import { call, put, select } from "redux-saga/effects";
import CarService from "../services/cars";
import {
  fetchCarsSucceeded,
  submitCarSucceeded,
  fetchCarSucceeded,
  deleteCarSucceeded,
} from "../actions/cars";

export function* fetchCars({ filter }) {
  const cars = yield call(CarService.apiCall, filter);
  yield put(fetchCarsSucceeded(cars));
}

export function* submitCar() {
  const { currentCars } = yield select((state) => state.cars);
  const { status, data } = yield call(CarService.apiSave, currentCars);
  yield put(submitCarSucceeded(status, data));
}

export function* fetchCar({ id }) {
  const car = yield call(CarService.apiCallOne, id);
  yield put(fetchCarSucceeded(car));
}

export function* deleteCar({ id }) {
  yield call(CarService.apiDelete, id);
  yield put(deleteCarSucceeded(true));
  yield call(fetchCars, {});
}
