import { call, put, select } from "redux-saga/effects";
import ClubService from "../services/clubs";
import {
  fetchClubsSucceeded,
  submitClubSucceeded,
  fetchClubSucceeded,
  deleteClubSucceeded,
} from "../actions/clubs";

export function* fetchClubs({ filter }) {
  const clubs = yield call(ClubService.apiCall, filter);
  yield put(fetchClubsSucceeded(clubs));
}

export function* submitClub() {
  const { currentClubs } = yield select((state) => state.clubs);
  const { status, data } = yield call(ClubService.apiSave, currentClubs);
  yield put(submitClubSucceeded(status, data));
}

export function* fetchClub({ id }) {
  const club = yield call(ClubService.apiCallOne, id);
  yield put(fetchClubSucceeded(club));
}

export function* deleteClub({ id }) {
  yield call(ClubService.apiDelete, id);
  yield put(deleteClubSucceeded(true));
  yield call(fetchClubs, {});
}
