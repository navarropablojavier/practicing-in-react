import { call, put, select } from "redux-saga/effects";
import CountryService from "../services/countries";
import {
  fetchCountriesSucceeded,
  submitContrySucceeded,
  fetchCountrySucceeded,
  deleteCountrySucceeded,
} from "../actions/countries";

export function* fetchCountries({ filter }) {
  const countries = yield call(CountryService.apiCall, filter);
  yield put(fetchCountriesSucceeded(countries));
}

export function* submitCountry() {
  const { currentCountries } = yield select((state) => state.countries);
  const { status, data } = yield call(CountryService.apiSave, currentCountries);
  yield put(submitContrySucceeded(status, data));
}

export function* fetchCountry({ id }) {
  const country = yield call(CountryService.apiCallOne, id);
  yield put(fetchCountrySucceeded(country));
}

export function* deleteCountry({ id }) {
  yield call(CountryService.apiDelete, id);
  yield put(deleteCountrySucceeded(true));
  yield call(fetchCountries, {});
}
