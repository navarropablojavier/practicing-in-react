export const FETCH_CARS_REQUESTED = "FETCH_CARS_REQUESTED"; //ACCION
export const FETCH_CARS_SUCCEEDED = "FECTH_CARS_SUCCEEDED"; //REACCION DE LA ACCION

export const fetchCarsRequested = () => ({ type: FETCH_CARS_REQUESTED }); //Disparadores
export const fetchCarsSucceeded = (cars) => ({
  type: FETCH_CARS_SUCCEEDED,
  cars,
}); //disparadoress

export const FETCH_CAR_REQUESTED = "FETCH_CAR_REQUESTED"; //ACCION
export const FETCH_CAR_SUCCEEDED = "FECTH_CAR_SUCCEEDED"; //REACCION DE LA ACCION

export const fetchCarRequested = (id) => ({ type: FETCH_CAR_REQUESTED, id }); //Disparadores
export const fetchCarSucceeded = (car) => ({ type: FETCH_CAR_SUCCEEDED, car }); //disparadoress

export const UPDATE_CARS = "UPDATE_CARS";
export const updateCars = (car) => ({ type: UPDATE_CARS, car });

export const SUBMIT_CAR_REQUESTED = "SUBMIT_CAR_REQUESTED";
export const submitCarRequested = () => ({ type: SUBMIT_CAR_REQUESTED });

export const SUBMIT_CAR_SUCCEEDED = "SUBMIT_CAR_SUCCEEDED";
export const submitCarSucceeded = (status, data) => ({
  type: SUBMIT_CAR_SUCCEEDED,
  status,
  data,
});

export const DELETE_CAR_REQUESTED = "DELETE_CAR_REQUESTED";
export const DELETE_CAR_SUCCEEDED = "DELETE_CAR_SUCCEEDED";

export const deleteCarRequested = (id) => ({ type: DELETE_CAR_REQUESTED, id });
export const deleteCarSucceeded = () => ({ type: DELETE_CAR_SUCCEEDED });
