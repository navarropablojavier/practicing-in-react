export const FETCH_COUNTRIES_REQUESTED = "FETCH_COUNTRIES_REQUESTED"; //ACCION
export const FETCH_COUNTRIES_SUCCEEDED = "FECTH_COUNTRIES_SUCCEEDED"; //REACCION DE LA ACCION

export const fetchCountriesRequested = () => ({
  type: FETCH_COUNTRIES_REQUESTED,
}); //Disparadores
export const fetchCountriesSucceeded = (countries) => ({
  type: FETCH_COUNTRIES_SUCCEEDED,
  countries,
}); //disparadoress

export const FETCH_COUNTRY_REQUESTED = "FETCH_COUNTRY_REQUESTED"; //ACCION
export const FETCH_COUNTRY_SUCCEEDED = "FECTH_COUNTRY_SUCCEEDED"; //REACCION DE LA ACCION

export const fetchCountryRequested = (id) => ({
  type: FETCH_COUNTRY_REQUESTED,
  id,
}); //Disparadores
export const fetchCountrySucceeded = (country) => ({
  type: FETCH_COUNTRY_SUCCEEDED,
  country,
}); //disparadoress

export const UPDATE_COUNTRIES = "UPDATE_COUNTRIES";
export const updateCountries = (country) => ({
  type: UPDATE_COUNTRIES,
  country,
});

export const SUBMIT_COUNTRY_REQUESTED = "SUBMIT_COUNTRY_REQUESTED";
export const submitCountryRequested = () => ({
  type: SUBMIT_COUNTRY_REQUESTED,
});

export const SUBMIT_COUNTRY_SUCCEEDED = "SUBMIT_COUNTRY_SUCCEEDED";
export const submitContrySucceeded = (status, data) => ({
  type: SUBMIT_COUNTRY_SUCCEEDED,
  status,
  data,
});

export const DELETE_COUNTRY_REQUESTED = "DELETE_COUNTRY_REQUESTED";
export const DELETE_COUNTRY_SUCCEEDED = "DELETE_COUNTRY_SUCCEEDED";

export const deleteCountryRequested = (id) => ({
  type: DELETE_COUNTRY_REQUESTED,
  id,
});
export const deleteCountrySucceeded = () => ({
  type: DELETE_COUNTRY_SUCCEEDED,
});
