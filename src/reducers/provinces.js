import {
  FETCH_PROVINCES_REQUESTED,
  FETCH_PROVINCES_SUCCEEDED,
  FETCH_PROVINCE_REQUESTED,
  FETCH_PROVINCE_SUCCEEDED,
  UPDATE_PROVINCES,
} from "../actions/provinces";

const initialState = {
  provinces: [],
  currentProvinces: {
    name: "",
    code: "",
  },
  headers: [
    {
      label: "Nombre",
      key: "name",
    },
    {
      label: "Codigo",
      key: "code",
    },
  ],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_PROVINCES_REQUESTED:
      return { ...state, provinces: [] };
    case FETCH_PROVINCES_SUCCEEDED:
      return { ...state, provinces: action.provinces };
    case FETCH_PROVINCE_REQUESTED:
      return { ...state, currentProvinces: initialState.currentProvinces };
    case FETCH_PROVINCE_SUCCEEDED:
      return { ...state, currentProvinces: action.province };
    case UPDATE_PROVINCES:
      return { ...state, currentProvinces: action.province };
    default:
      return { ...state };
  }
};
