import {
  FETCH_CARS_REQUESTED,
  FETCH_CARS_SUCCEEDED,
  FETCH_CAR_REQUESTED,
  FETCH_CAR_SUCCEEDED,
  UPDATE_CARS,
} from "../actions/cars";

const initialState = {
  cars: [],
  currentCars: {
    name: "",
    code: "",
  },
  headers: [
    {
      label: "Marca",
      key: "name",
    },
    {
      label: "Modelo",
      key: "code",
    },
  ],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_CARS_REQUESTED:
      return { ...state, cars: [] };
    case FETCH_CARS_SUCCEEDED:
      return { ...state, cars: action.cars };
    case FETCH_CAR_REQUESTED:
      return { ...state, currentCars: initialState.currentCars };
    case FETCH_CAR_SUCCEEDED:
      return { ...state, currentCars: action.car };
    case UPDATE_CARS:
      return { ...state, currentCars: action.car };
    default:
      return { ...state };
  }
};
