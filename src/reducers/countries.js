import {
  FETCH_COUNTRIES_REQUESTED,
  FETCH_COUNTRIES_SUCCEEDED,
  FETCH_COUNTRY_REQUESTED,
  FETCH_COUNTRY_SUCCEEDED,
  UPDATE_COUNTRIES,
} from "../actions/countries";

const initialState = {
  countries: [],
  currentCountries: {
    name: "",
    code: "",
  },
  headers: [
    {
      label: "Nombre",
      key: "name",
    },
    {
      label: "Codigo",
      key: "code",
    },
  ],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_COUNTRIES_REQUESTED:
      return { ...state, countries: [] };
    case FETCH_COUNTRIES_SUCCEEDED:
      return { ...state, countries: action.countries };
    case FETCH_COUNTRY_REQUESTED:
      return { ...state, currentCountries: initialState.currentCountries };
    case FETCH_COUNTRY_SUCCEEDED:
      return { ...state, currentCountries: action.country };
    case UPDATE_COUNTRIES:
      return { ...state, currentCountries: action.country };
    default:
      return { ...state };
  }
};
