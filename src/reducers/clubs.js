import {
  FETCH_CLUBS_REQUESTED,
  FETCH_CLUBS_SUCCEEDED,
  FETCH_CLUB_REQUESTED,
  FETCH_CLUB_SUCCEEDED,
  UPDATE_CLUBS,
} from "../actions/clubs";

const initialState = {
  clubs: [],
  currentClubs: {
    name: "",
    code: "",
  },
  headers: [
    {
      label: "Nombre",
      key: "name",
    },
    {
      label: "Codigo",
      key: "code",
    },
  ],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_CLUBS_REQUESTED:
      return { ...state, clubs: [] };
    case FETCH_CLUBS_SUCCEEDED:
      return { ...state, clubs: action.clubs };
    case FETCH_CLUB_REQUESTED:
      return { ...state, currentClubs: initialState.currentClubs };
    case FETCH_CLUB_SUCCEEDED:
      return { ...state, currentClubs: action.club };
    case UPDATE_CLUBS:
      return { ...state, currentClubs: action.club };
    default:
      return { ...state };
  }
};
