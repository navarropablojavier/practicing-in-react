import React, { Component } from "react";
import { connect } from "react-redux";
import { Form, Button, Container, Col, Row } from "reactstrap";
import { InputText } from "../../components/Forms";

import {
  updateCars,
  fetchCarRequested,
  submitCarRequested,
} from "../../actions/cars";

class Edit extends Component {
  //ciclo de vida de los componentes
  //coponentDiMount() este se activa cuando termina de renderizar muestra la app web
  componentDidMount() {
    const { id } = this.props.match.params;
    if (id) {
      this.props.fetchCar(id);
    }
  }

  handleChange(obj) {
    const { car } = this.props;
    Object.assign(car, obj);
    this.props.updateCar(car);
    this.forceUpdate();
  }
  render() {
    const {
      car: { name, code, id },
    } = this.props;
    return (
      <Container>
        <Row>
          <Col className="breadcrumb float-left mt-5" sm={{ size: 5 }}>
            <Form>
              <h1>{id ? "Modificar" : "Agregar"} Auto </h1>
              <hr />
              <InputText
                key="name"
                label="Marca: "
                value={name}
                placeholder="Escriba marca del auto"
                onChange={({ target: { value } }) =>
                  this.handleChange({ name: value })
                }
              />
              <InputText
                key="code"
                label="Modelo: "
                value={code}
                placeholder="Escriba modelo del auto"
                onChange={({ target: { value } }) =>
                  this.handleChange({ code: value })
                }
              />
              <Button
                outline
                color="info"
                size="lg"
                onClick={() => this.props.submit()}
              >
                {" "}
                Guardar
              </Button>
            </Form>
          </Col>
        </Row>
      </Container>
    );
  }
}

const mapStateToProps = (state) => ({
  car: state.cars.currentCars,
});

const mapDispactchToProps = (dispatch) => ({
  fetchCar: (id) => dispatch(fetchCarRequested(id)),
  updateCar: (car) => dispatch(updateCars(car)),
  submit: () => dispatch(submitCarRequested()),
});

export default connect(mapStateToProps, mapDispactchToProps)(Edit);
