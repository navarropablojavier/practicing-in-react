import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { Container, Row, Col, Button } from "reactstrap";

import Title from "../../components/Title";
import Table from "../../components/Table";

import { fetchCarsRequested, deleteCarRequested } from "../../actions/cars";

class Cars extends Component {
  componentDidMount() {
    this.props.requestCars();
  }

  render() {
    const { headers, documents } = this.props;
    return (
      <Container>
        <Row>
          <Col sm="8" className="float-left mt-5">
            <Title title="Autos" />
          </Col>
          <Col sm="4" className="float-right mt-5">
            <Button
              className="rounded-pill"
              tag={Link}
              color="btn btn-outline-dark"
              block="Block level button"
              to="/cars/new"
            >
              Nuevo auto
            </Button>
          </Col>
        </Row>
        <Row>
          <Col>
            <Table
              {...{ documents, headers, linkTo: "/cars" }}
              onDelete={(id) => this.props.deleteCar(id)}
            />
          </Col>
        </Row>
      </Container>
    );
  }
}
//mapStateToProps <<<<  todo el state del store esta aca, tomamos el store >>> Component como Props
//mapDdispatchToProps <<< todas las acciones que vamos a ejecutar o llamar >>> Component como Props
//mergeProps <<< funciona accion y propiedades onChange

const mapStateToProps = (state) => ({
  headers: state.cars.headers,
  documents: state.cars.cars,
});

const mapDispatchToProps = (dispatch) => ({
  requestCars: () => dispatch(fetchCarsRequested()),
  deleteCar: (id) => dispatch(deleteCarRequested(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Cars);
