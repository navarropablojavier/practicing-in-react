import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { Container, Row, Col, Button } from "reactstrap";

import Title from "../../components/Title";
import Table from "../../components/Table";

import {
  fetchProvincesRequested,
  deleteProvinceRequested,
} from "../../actions/provinces";

class Provinces extends Component {
  componentDidMount() {
    this.props.requestProvinces();
  }

  render() {
    const { headers, documents } = this.props;
    return (
      <Container>
        <Row>
          <Col sm="8" className="float-left mt-5">
            <Title title="Provincias" />
          </Col>
          <Col sm="4" className="float-right mt-5">
            <Button
              className="rounded-pill"
              tag={Link}
              color="btn btn-outline-dark"
              block="Block level button"
              to="/provinces/new"
            >
              Nuevo provincia
            </Button>
          </Col>
        </Row>
        <Row>
          <Col>
            <Table
              {...{ documents, headers, linkTo: "/provinces" }}
              onDelete={(id) => this.props.deleteProvince(id)}
            />
          </Col>
        </Row>
      </Container>
    );
  }
}

//mapStateToProps <<<<  todo el state del store esta aca, tomamos el store >>> Component como Props
//mapDdispatchToProps <<< todas las acciones que vamos a ejecutar o llamar >>> Component como Props
//mergeProps <<< funciona accion y propiedades onChange

const mapStateToProps = (state) => ({
  headers: state.provinces.headers,
  documents: state.provinces.provinces,
});

const mapDispatchToProps = (dispatch) => ({
  requestProvinces: () => dispatch(fetchProvincesRequested()),
  deleteProvince: (id) => dispatch(deleteProvinceRequested(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Provinces);
