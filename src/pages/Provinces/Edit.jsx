import React, { Component } from "react";
import { connect } from "react-redux";
import { InputText } from "../../components/Forms";
import { Form, Button, Container, Col, Row } from "reactstrap";

import {
  updateProvinces,
  fetchProvinceRequested,
  submitProvinceRequested,
} from "../../actions/provinces";

class Edit extends Component {
  //ciclo de vida de los componentes
  //coponentDiMount() este se activa cuando termina de renderizar muestra la app web
  componentDidMount() {
    const { id } = this.props.match.params;
    if (id) {
      this.props.fetchProvince(id);
    }
  }

  handleChange(obj) {
    const { province } = this.props;
    Object.assign(province, obj);
    this.props.updateProvince(province);
    this.forceUpdate();
  }
  render() {
    const {
      province: { name, code, id },
    } = this.props;
    return (
      <Container>
        <Row>
          <Col className="breadcrumb float-left mt-5" sm={{ size: 5 }}>
            <Form>
              <h1>{id ? "Modificar" : "Nueva"} Provincia </h1>
              <hr />
              <InputText
                key="name"
                label="Nombre: "
                value={name}
                placeholder="Escriba nombre de la provincia"
                onChange={({ target: { value } }) =>
                  this.handleChange({ name: value })
                }
              />
              <InputText
                key="code"
                label="Código: "
                value={code}
                placeholder="Escriba código de la provincia"
                onChange={({ target: { value } }) =>
                  this.handleChange({ code: value })
                }
              />
              <Button
                outline
                color="info"
                size="lg"
                onClick={() => this.props.submit()}
              >
                {" "}
                Guardar
              </Button>
            </Form>
          </Col>
        </Row>
      </Container>
    );
  }
}

const mapStateToProps = (state) => ({
  province: state.provinces.currentProvinces,
});

const mapDispactchToProps = (dispatch) => ({
  fetchProvince: (id) => dispatch(fetchProvinceRequested(id)),
  updateProvince: (province) => dispatch(updateProvinces(province)),
  submit: () => dispatch(submitProvinceRequested()),
});

export default connect(mapStateToProps, mapDispactchToProps)(Edit);
