import React, { Component } from "react";
import { connect } from "react-redux";
import { Form, Button, Container, Col, Row } from "reactstrap";
import { InputText, InputEmail } from "../../components/Forms";
import Title from "../../components/Title";

import {
  updatePersons,
  fetchPersonRequested,
  submitPersonRequested,
} from "../../actions/persons";

class Edit extends Component {
  //ciclo de vida de los componentes
  //coponentDiMount() este se activa cuando termina de renderizar muestra la app web
  componentDidMount() {
    const { id } = this.props.match.params;
    if (id) {
      this.props.fetchPerson(id);
    }
  }

  handleChange(obj) {
    const { person } = this.props;
    Object.assign(person, obj);
    this.props.updatePerson(person);
    this.forceUpdate();
  }

  render() {
    const {
      person: { name, surname, email, gender, avatar, id },
    } = this.props;
    return (
      <Container>
        <Row>
          <Col className="breadcrumb float-left mt-5" sm={{ size: 5 }}>
            <Form>
              <h1>{id ? "Modificar" : "Agregar"} Persona</h1>
              <hr />
              <InputText
                key="name"
                label="Nombre: "
                value={name}
                placeholder="Escriba su nombre"
                onChange={({ target: { value } }) =>
                  this.handleChange({ name: value })
                }
              />
              <InputText
                key="surname"
                label="Apellido: "
                value={surname}
                placeholder="Escriba su apellido"
                onChange={({ target: { value } }) =>
                  this.handleChange({ surname: value })
                }
              />
              <InputEmail
                key="email"
                label="Email: "
                value={email}
                placeholder="Escriba su e-mail"
                onChange={({ target: { value } }) =>
                  this.handleChange({ email: value })
                }
              />
              <InputText
                key="gender"
                label="Genero: "
                value={gender}
                placeholder="Escriba su genero"
                onChange={({ target: { value } }) =>
                  this.handleChange({ gender: value })
                }
              />
              <InputText
                key="avatar"
                label="Avatar: "
                value={avatar}
                placeholder="Escriba la dirección de su avatar"
                onChange={({ target: { value } }) =>
                  this.handleChange({ avatar: value })
                }
              />
              <Button
                outline
                color="info"
                size="lg"
                onClick={() => this.props.submit()}
              >
                {" "}
                Guardar
              </Button>
            </Form>
          </Col>
        </Row>
      </Container>
    );
  }
}

const mapStateToProps = (state) => ({
  person: state.persons.currentPersons,
});

const mapDispactchToProps = (dispatch) => ({
  fetchPerson: (id) => dispatch(fetchPersonRequested(id)),
  updatePerson: (person) => dispatch(updatePersons(person)),
  submit: () => dispatch(submitPersonRequested()),
});

export default connect(mapStateToProps, mapDispactchToProps)(Edit);
