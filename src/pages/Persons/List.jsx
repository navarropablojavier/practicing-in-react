import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { Container, Row, Col, Button } from "reactstrap";

import Title from "../../components/Title";
import Table from "../../components/Table";

import {
  fetchPersonsRequested,
  deletePersonRequested,
} from "../../actions/persons";

class Persons extends Component {
  componentDidMount() {
    this.props.requestPersons();
  }

  render() {
    const { headers, documents } = this.props;
    return (
      <Container>
        <Row>
          <Col sm="8" className="float-left mt-5">
            <Title title="Personas" />
          </Col>
          <Col sm="4" className="float-right mt-5">
            <Button
              className="rounded-pill"
              tag={Link}
              color="btn btn-outline-dark"
              block="Block level button"
              to="/persons/new"
            >
              Nueva persona
            </Button>
          </Col>
        </Row>
        <Row>
          <Col>
            <Table
              {...{ documents, headers, linkTo: "/persons" }}
              onDelete={(id) => this.props.deletePerson(id)}
            />
          </Col>
        </Row>
      </Container>
    );
  }
}

//mapStateToProps <<<<  todo el state del store esta aca, tomamos el store >>> Component como Props
//mapDdispatchToProps <<< todas las acciones que vamos a ejecutar o llamar >>> Component como Props
//mergeProps <<< funciona accion y propiedades onChange

const mapStateToProps = (state) => ({
  headers: state.persons.headers,
  documents: state.persons.persons,
});

const mapDispatchToProps = (dispatch) => ({
  requestPersons: () => dispatch(fetchPersonsRequested()),
  deletePerson: (id) => dispatch(deletePersonRequested(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Persons);
