import React from "react";
import { Route, Switch } from "react-router-dom";
import Title from "../../components/Title";
import List from "./List";
import Edit from "./Edit";

const Persons = ({ match: { path } }) => (
  <Switch>
    <Route
      path={`${path}/new`}
      strict
      component={Edit}
      title="Agregar Persona"
    />
    <Route
      path={`${path}/:id`}
      strict
      component={Edit}
      title="Modificar Persona"
    />
    <Route path={`${path}`} component={List} />
  </Switch>
);

export default Persons;
