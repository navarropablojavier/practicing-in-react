import React, { Component } from "react";
import { connect } from "react-redux";
import { Form, Button, Container, Col, Row } from "reactstrap";
import { InputText } from "../../components/Forms";

import {
  updateCountries,
  fetchCountryRequested,
  submitCountryRequested,
} from "../../actions/countries";

class Edit extends Component {
  //ciclo de vida de los componentes
  //coponentDiMount() este se activa cuando termina de renderizar muestra la app web
  componentDidMount() {
    const { id } = this.props.match.params;
    if (id) {
      this.props.fetchCountry(id);
    }
  }

  handleChange(obj) {
    const { country } = this.props;
    Object.assign(country, obj);
    this.props.updateCountry(country);
    this.forceUpdate();
  }
  render() {
    const {
      country: { name, code, id },
    } = this.props;
    return (
      <Container>
        <Row>
          <Col className="breadcrumb float-left mt-5" sm={{ size: 5 }}>
            <Form>
              <h1>{id ? "Modificar" : "Agregar"} Paìs</h1>
              <hr />
              <InputText
                key="name"
                label="Nombre: "
                value={name}
                placeholder="Escriba nombre del país"
                onChange={({ target: { value } }) =>
                  this.handleChange({ name: value })
                }
              />
              <InputText
                key="code"
                label="Código: "
                value={code}
                placeholder="Escriba código del país"
                onChange={({ target: { value } }) =>
                  this.handleChange({ code: value })
                }
              />
              <Button
                outline
                color="info"
                size="lg"
                onClick={() => this.props.submit()}
              >
                {" "}
                Guardar
              </Button>
            </Form>
          </Col>
        </Row>
      </Container>
    );
  }
}

const mapStateToProps = (state) => ({
  country: state.countries.currentCountries,
});

const mapDispactchToProps = (dispatch) => ({
  fetchCountry: (id) => dispatch(fetchCountryRequested(id)),
  updateCountry: (country) => dispatch(updateCountries(country)),
  submit: () => dispatch(submitCountryRequested()),
});

export default connect(mapStateToProps, mapDispactchToProps)(Edit);
