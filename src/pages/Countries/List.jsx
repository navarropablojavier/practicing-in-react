import React, { Component } from "react";
import { connect } from "react-redux";
import { Container, Row, Col, Button } from "reactstrap";

import Title from "../../components/Title";
import Table from "../../components/Table";
import { Link } from "react-router-dom";
import {
  fetchCountriesRequested,
  deleteCountryRequested,
} from "../../actions/countries";

class Countries extends Component {
  componentDidMount() {
    this.props.requestCountries();
  }

  render() {
    const { headers, documents } = this.props;
    return (
      <Container>
        <Row>
          <Col sm="8" className="float-left mt-5">
            <Title title="Paises" />
          </Col>
          <Col sm="4" className="float-right mt-5">
            <Button
              className="rounded-pill"
              tag={Link}
              color="btn btn-outline-dark"
              block="Block level button"
              to="/countries/new"
            >
              Nuevo país
            </Button>
          </Col>
        </Row>
        <Row>
          <Col>
            <Table
              {...{ documents, headers, linkTo: "/countries" }}
              onDelete={(id) => this.props.deleteCountry(id)}
            />
          </Col>
        </Row>
      </Container>
    );
  }
}

//mapStateToProps <<<<  todo el state del store esta aca, tomamos el store >>> Component como Props
//mapDdispatchToProps <<< todas las acciones que vamos a ejecutar o llamar >>> Component como Props
//mergeProps <<< funciona accion y propiedades onChange

const mapStateToProps = (state) => ({
  headers: state.countries.headers,
  documents: state.countries.countries,
});

const mapDispatchToProps = (dispatch) => ({
  requestCountries: () => dispatch(fetchCountriesRequested()),
  deleteCountry: (id) => dispatch(deleteCountryRequested(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Countries);
