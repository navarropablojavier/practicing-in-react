import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { Container, Row, Col, Button } from "reactstrap";

import Title from "../../components/Title";
import Table from "../../components/Table";

import { fetchClubsRequested, deleteClubRequested } from "../../actions/clubs";

class Clubs extends Component {
  componentDidMount() {
    this.props.requestClubs();
  }

  render() {
    const { headers, documents } = this.props;
    return (
      <Container>
        <Row>
          <Col sm="8" className="float-left mt-5">
            <Title title="Clubes" />
          </Col>
          <Col sm="4" className="float-right mt-5">
            <Button
              className="rounded-pill"
              tag={Link}
              color="btn btn-outline-dark"
              block="Block level button"
              to="/clubs/new"
            >
              Nuevo club
            </Button>
          </Col>
        </Row>
        <Row>
          <Col>
            <Table
              {...{ documents, headers, linkTo: "/clubs" }}
              onDelete={(id) => this.props.deleteClub(id)}
            />
          </Col>
        </Row>
      </Container>
    );
  }
}

//mapStateToProps <<<<  todo el state del store esta aca, tomamos el store >>> Component como Props
//mapDdispatchToProps <<< todas las acciones que vamos a ejecutar o llamar >>> Component como Props
//mergeProps <<< funciona accion y propiedades onChange

const mapStateToProps = (state) => ({
  headers: state.clubs.headers,
  documents: state.clubs.clubs,
});

const mapDispatchToProps = (dispatch) => ({
  requestClubs: () => dispatch(fetchClubsRequested()),
  deleteClub: (id) => dispatch(deleteClubRequested(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Clubs);
