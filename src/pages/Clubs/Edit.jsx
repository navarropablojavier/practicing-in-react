import React, { Component } from "react";
import { connect } from "react-redux";
import { Form, Button, Container, Col, Row } from "reactstrap";
import { InputText } from "../../components/Forms";

import {
  updateClubs,
  fetchClubRequested,
  submitClubRequested,
} from "../../actions/clubs";

class Edit extends Component {
  //ciclo de vida de los componentes
  //coponentDiMount() este se activa cuando termina de renderizar muestra la app web
  componentDidMount() {
    const { id } = this.props.match.params;
    if (id) {
      this.props.fetchClub(id);
    }
  }

  handleChange(obj) {
    const { club } = this.props;
    Object.assign(club, obj);
    this.props.updateClub(club);
    this.forceUpdate();
  }
  render() {
    const {
      club: { name, code, id },
    } = this.props;
    return (
      <Container>
        <Row>
          <Col className="breadcrumb float-left mt-5" sm={{ size: 5 }}>
            <Form>
              <h1>{id ? "Modificar" : "Agregar"} Club</h1>
              <hr />
              <InputText
                key="name"
                label="Nombre: "
                value={name}
                placeholder="Escriba nombre del club"
                onChange={({ target: { value } }) =>
                  this.handleChange({ name: value })
                }
              />
              <InputText
                key="code"
                label="Código: "
                value={code}
                placeholder="Escriba código del club"
                onChange={({ target: { value } }) =>
                  this.handleChange({ code: value })
                }
              />
              <Button
                outline
                color="info"
                size="lg"
                onClick={() => this.props.submit()}
              >
                {" "}
                Guardar
              </Button>
            </Form>
          </Col>
        </Row>
      </Container>
    );
  }
}

const mapStateToProps = (state) => ({
  club: state.clubs.currentClubs,
});

const mapDispactchToProps = (dispatch) => ({
  fetchClub: (id) => dispatch(fetchClubRequested(id)),
  updateClub: (club) => dispatch(updateClubs(club)),
  submit: () => dispatch(submitClubRequested()),
});

export default connect(mapStateToProps, mapDispactchToProps)(Edit);
