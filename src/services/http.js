const API = "http://localhost:3001";

class HTTP {
  static async get(url) {
    const response = await fetch(`${API}/${url}`);
    try {
      if (response.ok) {
        return response.json();
      }
      return { error: true };
    } catch (err) {
      return { error: true };
    }
  }

  static async delete(url) {
    const response = await fetch(`${API}/${url}`, { method: "delete" });
    try {
      if (response.ok) {
        return response.json();
      }
      return { error: true };
    } catch (err) {
      return { error: true };
    }
  }

  static async put(url, obj) {
    const response = await fetch(`${API}/${url}`, {
      method: "put",
      body: JSON.stringify(obj),
      headers: { "Content-Type": "application/json; charset=UTF-8" },
    });
    try {
      if (response.ok) {
        return response.json();
      }
      return { error: true };
    } catch (err) {
      return { error: true };
    }
  }
  static async post(url, obj) {
    const response = await fetch(`${API}/${url}`, {
      method: "post",
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
      body: JSON.stringify(obj),
      headers: { "Content-Type": "application/json; charset=UTF-8" },
    });
    try {
      if (response.ok) {
        return response.json();
      }
      return { error: true };
    } catch (err) {
      return { error: true };
    }
  }
}

export default HTTP;
