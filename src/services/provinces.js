import HTTP from "./http";
const API = "api/provinces";

export default class Province {
  static async apiCall() {
    return HTTP.get(API);
  }

  static async apiCallOne(id) {
    return HTTP.get(`${API}/${id}`);
  }

  static async apiSave(province) {
    if (province.id) {
      return HTTP.put(`${API}/${province.id}`, province);
    } else {
      return HTTP.post(API, province);
    }
  }

  static async apiDelete(id) {
    return HTTP.delete(`${API}/${id}`);
  }
}
