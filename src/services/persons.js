import HTTP from "./http";
const API = "api/persons";

export default class Person {
  static async apiCall() {
    return HTTP.get(API);
  }

  static async apiCallOne(id) {
    return HTTP.get(`${API}/${id}`);
  }

  static async apiSave(person) {
    if (person.id) {
      return HTTP.put(`${API}/${person.id}`, person);
    } else {
      return HTTP.post(API, person);
    }
  }

  static async apiDelete(id) {
    return HTTP.delete(`${API}/${id}`);
  }
}
