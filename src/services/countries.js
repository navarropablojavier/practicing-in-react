import HTTP from "./http";
const API = "api/country";

export default class Country {
  static async apiCall() {
    return HTTP.get(API);
  }

  static async apiCallOne(id) {
    return HTTP.get(`${API}/${id}`);
  }

  static async apiSave(country) {
    if (country.id) {
      return HTTP.put(`${API}/${country.id}`, country);
    } else {
      return HTTP.post(API, country);
    }
  }

  static async apiDelete(id) {
    return HTTP.delete(`${API}/${id}`);
  }
}
