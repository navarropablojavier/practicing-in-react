import HTTP from "./http";
const API = "api/clubs";

export default class Club {
  static async apiCall() {
    return HTTP.get(API);
  }

  static async apiCallOne(id) {
    return HTTP.get(`${API}/${id}`);
  }

  static async apiSave(club) {
    if (club.id) {
      return HTTP.put(`${API}/${club.id}`, club);
    } else {
      return HTTP.post(API, club);
    }
  }

  static async apiDelete(id) {
    return HTTP.delete(`${API}/${id}`);
  }
}
