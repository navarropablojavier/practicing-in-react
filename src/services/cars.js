import HTTP from "./http";
const API = "api/cars";

export default class Car {
  static async apiCall() {
    return HTTP.get(API);
  }

  static async apiCallOne(id) {
    return HTTP.get(`${API}/${id}`);
  }

  static async apiSave(car) {
    if (car.id) {
      return HTTP.put(`${API}/${car.id}`, car);
    } else {
      return HTTP.post(API, car);
    }
  }

  static async apiDelete(id) {
    return HTTP.delete(`${API}/${id}`);
  }
}
