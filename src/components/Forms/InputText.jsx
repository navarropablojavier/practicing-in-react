import React from "react";
import { FormGroup, Label, Input } from "reactstrap";

const InputText = ({ ...props }) => (
  <FormGroup>
    {props.label && <Label>{props.label}</Label>}
    <Input type="text" {...props} />
  </FormGroup>
);

export default InputText;
