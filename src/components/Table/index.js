import React from "react";
import { Link } from "react-router-dom";

import { Table, Button, Media } from "reactstrap";

const TableComponent = ({
  documents,
  headers,
  linkTo,
  onDelete,
  primaryKey = "id",
}) => (
  <Table size="sm" hover responsive>
    <thead>
      <tr>
        {headers && headers.map((header) => <th>{header.label}</th>)}
        {linkTo && <th> Acciones </th>}
      </tr>
    </thead>
    <tbody>
      {documents &&
        headers &&
        documents.map((document) => (
          <tr>
            {headers.map((header) => (
              <td>
                {header.media && <Media src={document[header.key]} />}
                {!header.media && document[header.key]}
              </td>
            ))}
            <td>
              {linkTo && (
                <Button
                  block="Block level button"
                  outline
                  color="btn btn-outline-primary"
                  size="sm"
                  tag={Link}
                  to={`${linkTo}/${document[primaryKey]}`}
                >
                  {" "}
                  Editar{" "}
                </Button>
              )}
              {onDelete && (
                <Button
                  block="Block level button"
                  outline
                  color="btn btn-outline-danger"
                  size="sm"
                  onClick={() => onDelete(document[primaryKey])}
                >
                  {" "}
                  Borrar{" "}
                </Button>
              )}
            </td>
          </tr>
        ))}
    </tbody>
  </Table>
);

export default TableComponent;
