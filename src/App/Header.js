import React, { useState } from "react";
import { Link } from "react-router-dom";
import logo from "./images/Logo_Indec.png";

import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
} from "reactstrap";

const Header = () => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  return (
    <header>
      <Navbar
        className="navbar-right "
        className=" navbar fixed-top navbar-expand-lg navbar-dark bg-primary"
      >
        <NavbarBrand tag={Link} to="/">
          <img src={logo} height="40" />
          INDEC
        </NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="ml-auto" navbar>
            <NavItem>
              <NavLink tag={Link} to="/countries">
                Paises
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink tag={Link} to="/persons">
                Personas
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink tag={Link} to="/cars">
                Autos
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink tag={Link} to="/provinces">
                Provincias
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink tag={Link} to="/clubs">
                Clubes
              </NavLink>
            </NavItem>
          </Nav>
        </Collapse>
      </Navbar>
      <br />
      <br />
    </header>
  );
};

export default Header;
