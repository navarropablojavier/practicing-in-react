import React from "react";
import { Card, CardText } from "reactstrap";

const footer = (props) => {
  return (
    <footer>
      <Card
        body
        inverse
        style={{ backgroundColor: "#333", borderColor: "#555" }}
      >
        <br />
        <h1>Información del sitio web</h1>
        <CardText>Develop del INDEC.</CardText>
        <CardText>Nombre: Pablo Javier Navarro.</CardText>
        <CardText>Email: pnavarro@indec.gob.ar </CardText>{" "}
        <CardText>Tel: 15-5904-6244.</CardText>
      </Card>
    </footer>
  );
};

export default footer;
