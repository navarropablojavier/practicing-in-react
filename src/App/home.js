import React from "react";
import { UncontrolledCarousel } from "reactstrap";

import customImage2 from "./images/indec1.jpg";
import customImage3 from "./images/indec3.jpg";

const items = [
  {
    src: customImage2,
    key: "1",
  },
  {
    src: customImage3,
    key: "2",
  },
];

const Example = () => <UncontrolledCarousel items={items} />;

export default Example;
